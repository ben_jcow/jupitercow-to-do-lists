jQuery(document).ready(function($) {
	$('#todo-authored li').click(loadTodoDefinition);
	$('#todo-admin li').click(loadTodoDefinition);
	function loadTodoDefinition() {
		$me = $(this);
		$parentlist = $me.parents('ul');

		$.post(
			jcow_ajax.ajaxurl,
			{
				'action': 'jcow_get_todo',
				'post_id': $me.data('post')
			},
			function(response) {
				if (response) {
					var listdata = JSON.parse(response);
					if (listdata &&
						('undefined' != typeof listdata.title) &&
						('undefined' != typeof listdata.permalink) &&
						('undefined' != typeof listdata.data) &&
						('undefined' != typeof listdata.users) &&
						('undefined' != typeof listdata.assignee)
					) {
						$('#todo-nav li').removeClass('selected');
						$me.addClass('selected');
						$parentlist.attr('data-active', $me.data('post'));

						var $container = $('#todo-detail');
						$container.empty();

						var h1 = document.createElement('h1');
						h1.appendChild(document.createElement('input'));
						h1.lastChild.setAttribute('name', 'title');
						h1.lastChild.setAttribute('type', 'text');
						h1.lastChild.setAttribute('value', listdata.title);
						$container.append(h1);

						var p = document.createElement('p');
						p.appendChild(document.createElement('label'));
						p.lastChild.setAttribute('for', 'assignee');
						p.lastChild.appendChild(document.createTextNode('Assigned to: '));
						p.appendChild(document.createElement('select'));
						p.lastChild.id = 'assignee';
						p.lastChild.setAttribute('name', 'assignee');
						p.lastChild.appendChild(document.createElement('option'));
						p.lastChild.lastChild.setAttribute('value', '');
						p.lastChild.lastChild.appendChild(document.createTextNode('Unassigned'));
						for (i in listdata.users) {
							p.lastChild.appendChild(document.createElement('option'));
							p.lastChild.lastChild.setAttribute('value', i);
							if (i == listdata.assignee) {
								p.lastChild.lastChild.setAttribute('selected', true);
							}
							p.lastChild.lastChild.appendChild(document.createTextNode(listdata.users[i]));
						}
						$container.append(p);

						var list = document.createElement('ul');
						var $list = $(list);
						for (var i in listdata.data) {
							addNewTodoItem($list, listdata.data[i]);
						}
						addNewTodoItem($list);

						$container.append(list);
						$list.sortable();

						var submit = document.createElement('button');
						submit.appendChild(document.createTextNode('Save To Do List'));
						$container.append(submit);
						// fwd todo to server for saving
						$(submit).click( function() {
							var postdata = {
										'action': 'jcow_save_todo',
										'post_id': $me.data('post'),
										'title': $('h1 input[name="title"]').val(),
										'assignee': $('#assignee').val(),
										'data': []
									};
							$container.find('ul input').each(function() {
								if (this.value) {
									postdata.data.push(this.value);
								}
							});
							if (0 == postdata.data.length) {
								alert('Lists must contain at least one item.');
							} else {
								$.post(
									jcow_ajax.ajaxurl,
									postdata,
									function(answer) {
										if (parseInt(answer) > 0) {
											if ('new' == postdata.post_id) {
												var li = document.createElement('li')
												li.setAttribute('data-post', answer);
												li.className = 'selected';
												li.appendChild(document.createTextNode(postdata.title));
												$(li).click(loadTodoDefinition);
												$('#todo-nav li').removeClass('selected');
												$parentlist.find('li:last-child').before(li);
												$(li).trigger('click');
											} else {
												$me.empty();
												$me.append(document.createTextNode(postdata.title));
											}
											alert('To Do List saved');
										} else {
											alert('There was an error saving this list');
										}
									}
								);
							}
						});

						if ('new' != $me.data('post')) {
							var del = document.createElement('button');
							del.appendChild(document.createTextNode('Delete To Do List'));
							$container.append(del);
							$(del).click( function() {
								var postdata = {
									'action': 'jcow_save_todo',
									'post_id': $me.data('post'),
									'title': $('h1 input[name="title"]').val(),
									'assignee': '',
									'data': []
								};
								$.post(
									jcow_ajax.ajaxurl,
									postdata,
									function(answer) {
										$me.remove();
										$container.empty();
										alert('To Do List deleted');
									}
								);
							});

							var view = document.createElement('button');
							view.className = 'viewList';
							view.appendChild(document.createTextNode('View To Do List'));
							$container.append(view);
							$(view).click( function() {
								document.location.href = listdata.permalink;
							});
						}
					}
				}
			}
		);
	}

	// create new list
	function addNewTodoItem($ul, val) {
		if (!$ul) {
			var $ul = $('#todo-detail ul');
		}

		if (!val) {
			var val = '';
		}

		var li = document.createElement('li');
		li.appendChild(document.createElement('input'));
		li.lastChild.setAttribute('type', 'text');

		if (val) {
			li.lastChild.value = val;
		} else {
			$(li.lastChild).change(function() {
				$(this).off('change');
				addNewTodoItem($ul);
			});
		}

		$ul.append(li);
		$ul.sortable();
	}

	$('#jcowSaveTodo').click( function() {
		$me = $(this);
        var checks = [];
        $me.parents('form').find('input[type="checkbox"]').each( function() {
            checks.push(this.checked);
        });
        $.post(
            jcow_ajax.ajaxurl,
            {
                'action': 'jcow_save_todo_checks',
                'post_id': $me.data('id'),
                'checks': checks
            },
            function(response) {
                if (response) {
                    alert('List saved.');
                } else {
                    alert('Error saving. Please try again.');
                }
            }
        );
	});
});

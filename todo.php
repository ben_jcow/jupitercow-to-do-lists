<?php
/**
 * Plugin Name: Jupitercow To Dos
 * Plugin URI: http://jcow.com
 * Description: Single-use checklists
 * Author: Ben Coy
 * Author URI: http://jcow.com
 */
class jcow_todo {
	function __construct() {
		// set everything up
		add_action('admin_menu', array($this, 'menu_page'));
		add_action('init', array($this, 'todo_post_types'));
		add_action('wp_enqueue_scripts', array($this, 'add_chrome'));
		add_action('admin_enqueue_scripts', array($this, 'add_chrome'));

		// ajax calls
		add_action( 'wp_ajax_jcow_get_todo', array($this, 'ajax_get_todo') );
		add_action( 'wp_ajax_jcow_save_todo', array($this, 'ajax_save_todo') );
		add_action( 'wp_ajax_jcow_save_todo_checks', array($this, 'ajax_save_todo_checks') );

		// handle front-end display
		add_filter( 'the_content', array($this, 'frontend_filter'), 1 );
	}

	/**
	 * we need to store both list types and list instances
	 */
	function todo_post_types() {
		register_post_type('jcow_todos',
			array(
				'labels' => array(
					'name' => 'To Do Lists',
					'singular_name' => 'To Do List'
				),
				'public' => true,
				'show_ui' => false,
				'show_in_nav_menus' => false,
				'show_in_menu' => false,
				'exclude_from_search' => true,
				'has_archive' => true,
				'rewrite' => array('slug' => 'to_do'),
				'supports' => array('title', 'editor', 'revisions')
			)
		);
	}

	/**
	 * AJAX calls
	 */
	function ajax_get_todo() {
		$users_q = new WP_User_Query(array(
			// 'exclude' => get_current_user_id(),
			'orderby' => 'display_name',
			'number' => 999
		));
		$users = array();
		if ( ! empty( $users_q->results ) ) {
			foreach ( $users_q->results as $u ) {
				$users[$u->ID] = $u->display_name;
			}
		}
		
		if ($post = get_post($_POST['post_id'])) {
			$content = unserialize($post->post_content);
			if ($content) {
				$data = array_values(unserialize($post->post_content));
			} else {
				$data = array();
			}
			echo json_encode(
				array(
					'title' => $post->post_title,
					'permalink' => get_permalink($post->ID),
					'data' => $data,
					'users' => $users,
					'assignee' => get_post_meta($post->ID, 'assignee', true)
				)
			);
		} else {
			echo json_encode(
				array('title' => 'New To Do List',
					'permalink' => '',
					'data' => array(),
					'users' => $users,
					'assignee' => ''
				)
			);
		}
		die();
	}
	function ajax_save_todo() {
		if (
			!empty($_POST['post_id']) &&
			!empty($_POST['title']) &&
			isset($_POST['data']) &&
			isset($_POST['assignee'])
		) {
			$saved = array();
			if (
				$post = get_post($_POST['post_id']) &&
				is_array($data_r = unserialize($post->post_content))
			) {
				foreach ($data_r as $k => $v) {
					if (empty($saved[$v])) {
						$saved[$v] = array_keys($data_r, $v);
					}
				}
			}
			if ($post = get_post($_POST['post_id'])) {
				if (is_array($data_r = unserialize($post->post_content))) {
					foreach ($data_r as $k => $v) {
						if (empty($saved[$v])) {
							$saved[$v] = array_keys($data_r, $v);
						}
					}
				}
			}
			if ( !empty($_POST['data']) ) {
				$data = array();
				foreach ($_POST['data'] as $k => $v) {
					$ref = empty($saved[$v]) ? -1 : array_shift($saved[$v]);
					$coef = ($ref > 0) ? 1 : -1;
					$data[$coef * ($k+1)] = $v;
				}
				$post_id = wp_insert_post( array(
					'ID' => (('new' == $_POST['post_id']) ? '' : $_POST['post_id']),
					'post_type' => 'jcow_todos',
					'post_status' => 'publish',
					'post_content' => serialize($data),
					'post_title' => $_POST['title'],
					'post_name' => sanitize_title($_POST['title'])
				));
				update_post_meta($post_id, 'assignee', $_POST['assignee']);
				echo $post_id;
			} else {
				wp_trash_post( $_POST['post_id'] );
				echo '0';
			}
		}
		die();
	}
	function ajax_save_todo_checks() {
		if (
			!empty($_POST['post_id']) &&
			!empty($_POST['checks']) &&
			$post = get_post($_POST['post_id'])
		) {
			$saved = array_values(unserialize($post->post_content));
			$content = array();
			foreach ($_POST['checks'] as $k => $v) {
				$coef = ((!$v || 'false' == strtolower($v)) ? -1 : 1);
				$content[$coef * ($k+1)] = $saved[$k];
			}
			echo wp_update_post( array(
				'ID' => $_POST['post_id'],
				'post_content' => serialize($content)
			));
		}
	}

	/**
	 * get array of to do posts
	 */
	function get_todos($relation='') {
		$params = array();
		if ('assigned' == $relation) {
			$params['meta_key'] = 'assignee';
			$params['meta_value'] = get_current_user_id();
		} elseif ( ('admin' == $relation) && current_user_can( 'edit_others_posts' )) {
			$params['author'] = '-'.get_current_user_id();
		} else {
			$params['author'] = get_current_user_id();
		}
		return get_posts(
			array_merge(
				$params,
				array(
					'posts_per_page' => -1,
					'orderby' => 'title',
					'order' => 'ASC',
					'post_type' => 'jcow_todos'
				)
			)
		);
	}


	/**
	 * include script and styles
	 */
	function add_chrome() {
		wp_register_script( 'jcow_todos_js', plugins_url('todo.js', __FILE__), array('jquery') );
		wp_localize_script( 'jcow_todos_js', 'jcow_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php')));
		wp_enqueue_script( 'jcow_todos_js' );
		wp_enqueue_script( 'jcow_sortable_js', plugins_url('jquery.sortable.min.js', __FILE__), array('jquery') );
		wp_enqueue_style( 'jcow_todos_css', plugins_url('todo.css', __FILE__) );
	}

	/**
	 * create the panel on the admin side
	 */
	function menu_page() {
		add_menu_page('Jupitercow To Do Lists', 'To Do Lists', 'read', 'jcow_todos', array($this, 'admin_panel'));
	}
	function admin_panel() {
?>
		<div id="todo-wrap">
			<div id="todo-nav">
				<p>Your Assignments</p>
				<ul id="todo-assigned">
<?php
					$todos = $this->get_todos('assigned');
					foreach ($todos as $todo) {
						echo '<li><a href="'.get_permalink($todo->ID).'">'.$todo->post_title.'</a></li>';
					}
?>
				</ul>
				<p>Your Lists</p>
				<ul id="todo-authored">
<?php
					$todos = $this->get_todos();
					foreach ($todos as $todo) {
						echo '<li data-post="'.$todo->ID.'">'.$todo->post_title.'</li>';
					}
?>
					<li data-post="new" class="dashicons dashicons-plus"></li>
				</ul>
				<p>Administrator</p>
				<ul id="todo-admin">
<?php
					$todos = $this->get_todos('admin');
					foreach ($todos as $todo) {
						echo '<li data-post="'.$todo->ID.'">'.$todo->post_title.'</li>';
					}
?>
				</ul>
			</div>
			<form id="todo-detail" action="" method="post" onsubmit="return false;"></form>
		</div>
<?php
	}

	function frontend_filter($content) {
		global $post;
		if ('jcow_todos' == $post->post_type) {
			ob_start();
			$list = unserialize($post->post_content);
            echo '<form action="" method="post" onsubmit="return false">';
            echo '<ol id="jcow_list_instances">';
			if ($list) {
				foreach ($list as $k => $v) {
?>
					<li>
						<input type="checkbox"
							id="items_<?php echo $k; ?>"
							name="items[<?php echo $k; ?>]"
							<?php if ($k > 0) echo 'checked="true"'; ?>
						>
						<label for="item_<?php echo $k; ?>">
						<?php echo $v; ?>
						</label>
					</li>
<?php
				}
			}
			echo '</ol>';
			echo '<button id="jcowSaveTodo" data-id="'.$post->ID.'">Save</button>';
			echo '</form>';
			return str_replace("\n", '', ob_get_clean());
		} else return $content;
	}

}

if (empty($jcow_todo)) {
	$jcow_todo = new jcow_todo;
}
